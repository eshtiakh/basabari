<footer>

        <div class="container">
            <div class="row">
                <div class="footer-containertent">

                    <ul class="footer-social-info">
                        <li>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                        </li>
                    </ul>
					<br/><br/>
<p>Copyright © 2018. Template by: <a href="http://webthemez.com/free-bootstrap-templates/" title="Free Bootstrap Themes">WebThemez</a>, All rights reserved</p>
                </div>
            </div>
        </div>
    </footer>








    

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script type="text/javascript" src="<?php echo base_url();?>files/js/jquery-1.11.3.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/modernizr.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/jquery.easing.1.3.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/jquery.scrollUp.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/jquery.easypiechart.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/isotope.pkgd.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/jquery.fitvids.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/jquery.stellar.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/jquery.waypoints.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/jquery.nav.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/imagesloaded.pkgd.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/smooth-scroll.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/jquery.magnific-popup.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/jquery.sliderPro.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/js/owl.carousel.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>files/contact/jqBootstrapValidation.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>files/contact/contact_me.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>files/js/custom.js" ></script>



  </body>
</html>
